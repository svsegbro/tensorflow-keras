# Getting started

## Local installation

* Anaconda docs:
  * <https://docs.anaconda.com/>
  * <https://docs.anaconda.com/anaconda/install/>
* Install Anaconda:
  * `wget https://repo.anaconda.com/archive/Anaconda3-2020.02-Linux-x86_64.sh -O ~/anaconda.sh`
  * `sudo /bin/bash ~/anaconda.sh -b -p /opt/conda`
  * `rm ~/anaconda.sh`
  * `sudo ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh`
  * `echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc`
  * `echo "conda activate base" >> ~/.bashrc`
* Fix ownership of conda folders (otherwise you will get, for instance, a permission error when trying to create an enviroment)
  * `sudo chown 1000:1000 ~/.conda`
  * `sudo chown -R 1000:1000 /opt/conda/`
* Create environment
  * `conda create -n mytfenv`
* Activate environment
  * `conda activate mytfenv`
* Install pip installer:
  * `conda install pip`
* Install libraries:
  * Udemy provides a file *requirements.txt*
  * `pip install -r requirements.txt`
  * This results in an error: `ERROR: Could not find a version that satisfies the requirement tensorflow==2.0.0 (from -r requirements.txt (line 8)) (from versions: 2.2.0rc1, 2.2.0rc2)`
  * `conda install python==3.7`
  * Try again: `pip install -r requirements.txt`
* Start Jupyter Notebook:
  * `jupyter notebook`  

## Docker

* Official Anaconda Docker images: <https://hub.docker.com/r/continuumio/anaconda/>